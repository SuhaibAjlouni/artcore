Operations1​ Assistant App download redirect

This script is deployed at https://gitlab.com/cioplenu/operations1-assistant-download and its goal is to redirect the customer to the right download page, depending on his Operating System. (either Appstore, Google Play or just the Operations1 site)

By having a centralized link, it allows us to have a dynamic QR code